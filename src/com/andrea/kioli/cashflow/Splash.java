package com.andrea.kioli.cashflow;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;
import com.andrea.kioli.cashflow.utils.BetterActivity;
import com.andrea.kioli.cashflow.utils.Constants;

public class Splash extends BetterActivity {

	private MyCount counter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
	}

	/** Method called when the screen orientation changes */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		setContentView(R.layout.splash);
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onResume() {
		counter = new MyCount(Constants.SPLASH_TIME, Constants.SPLASH_STEP);
		counter.start();
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		counter.cancel();
	}

	public class MyCount extends CountDownTimer {

		public MyCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			finish();
			startActivity(new Intent(Splash.this, MainActivity.class));
		}

		@Override
		public void onTick(long millisUntilFinished) {
		}
	}
}