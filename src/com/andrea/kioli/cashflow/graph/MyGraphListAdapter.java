/** Copyright 2012 by Lorenzo Marchiori
 * 
 * List adapter that takes care of properly filling the list in the home page for the account movements
 * 
 * @author kioli */

package com.andrea.kioli.cashflow.graph;

import java.util.ArrayList;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.andrea.kioli.cashflow.R;
import com.andrea.kioli.cashflow.database.tables.Pigs.ListAccountsAll;
import com.andrea.kioli.cashflow.list_movement.AccountObject;
import com.andrea.kioli.cashflow.utils.Challenge;

public class MyGraphListAdapter extends ArrayAdapter<AccountObject> {

	private final ArrayList<AccountObject> items;
	private final Context context;
	private final Challenge challenge;
	private int accountID;
	private Cursor cursor;

	public MyGraphListAdapter(Challenge challenge, Context context, int textViewResourceId, ArrayList<AccountObject> items, int accountID) {
		super(context, textViewResourceId, items);
		this.items = items;
		this.context = context;
		this.challenge = challenge;
		this.accountID = accountID;
	}

	@Override
	public View getView(int index, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.graph_row, null);
		}
		
		AccountObject value = items.get(index);

		if (value != null) {
			TextView graphLine = (TextView) v.findViewById(R.id.graph_text);

			Double categ_money = value.getExpanse();
			cursor = context.getContentResolver().query(ListAccountsAll.TABLE_URI, new String[] { ListAccountsAll.TOTAL },
					"" + ListAccountsAll._ID + "=" + accountID + "", null, null);
			cursor.moveToNext();
			Double total = cursor.getDouble(cursor.getColumnIndex(ListAccountsAll.TOTAL));
			
			// set percentage
			if (graphLine != null) {
				graphLine.setText(String.valueOf((categ_money/total)*100));
				graphLine.setBackgroundColor(challenge.allColors[index]);
			}
		}
		return v;
	}
}