package com.andrea.kioli.cashflow.graph;

import java.util.ArrayList;
import java.util.HashMap;
import com.andrea.kioli.cashflow.R;
import com.andrea.kioli.cashflow.R.id;
import com.andrea.kioli.cashflow.R.layout;
import com.andrea.kioli.cashflow.database.tables.Movements.MovementsAll;
import com.andrea.kioli.cashflow.list_movement.AccountObject;
import com.andrea.kioli.cashflow.list_movement.MyListAdapter;
import com.andrea.kioli.cashflow.utils.Challenge;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MyGraph extends Activity {

	private Challenge challenge;
	private ListView list;
	private TextView name;
	private Cursor cursor;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		challenge = (Challenge) getApplication();
		
		initUI();
		init();
	}

	public void initUI() {
		setContentView(R.layout.graph);
		list = (ListView) findViewById(R.id.graph_list);
		name = (TextView) findViewById(R.id.graph_account);
	}
	
	public void init() {

		Intent intent = getIntent();
		String chosen_account = intent.getStringExtra("account");
		int chosen_accountID = intent.getIntExtra("accountID", 0);
		name.setText(chosen_account);

		cursor = this.getContentResolver().query(MovementsAll.TABLE_URI,
				new String[] { MovementsAll._ID, MovementsAll.VALUE, MovementsAll.CATEGORY },
				MovementsAll.PARENT_ACCOUNT_ID + "=" + chosen_accountID + "", null, MovementsAll._ID + " DESC");

		//HashMap<String, String> mapExpanses = new HashMap<String, String>(cursor.getCount());
		ArrayList<AccountObject> description_items = new ArrayList<AccountObject>();
		
		while (cursor.moveToNext()) {
			AccountObject myObject = new AccountObject();
			myObject.category = cursor.getInt(cursor.getColumnIndex(MovementsAll.CATEGORY));
			myObject.expanse = cursor.getDouble(cursor.getColumnIndex(MovementsAll.VALUE));
			description_items.add(myObject);
			//mapExpanses.put(cursor.getString(cursor.getColumnIndex(MovementsAll.CATEGORY)),
			//		String.valueOf(cursor.getDouble(cursor.getColumnIndex(MovementsAll.VALUE))));
		}

		// Updates the entries in the list with DB data
		MyGraphListAdapter list_adapter = new MyGraphListAdapter(challenge, this, R.layout.list_row_layout, description_items, chosen_accountID);
		list.setAdapter(list_adapter);
	}
}
