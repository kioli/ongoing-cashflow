package com.andrea.kioli.cashflow;

import java.text.DecimalFormat;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.andrea.kioli.cashflow.utils.BetterActivity;
import com.andrea.kioli.cashflow.utils.Challenge;
import com.andrea.kioli.cashflow.utils.Constants;

public class MultipleMovementActivity extends BetterActivity {

	Context context;
	Challenge challenge;
	SQLiteDatabase myDB;
	String account;
	Cursor cursor;
	EditText money, description;
	DecimalFormat twoDForm = new DecimalFormat("#.##");

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		challenge = (Challenge) getApplication();
		context = this;

		myDB = challenge.myDB;

		setContentView(R.layout.multiple_occurrences_layout);
		init();
	}

	public void init() {

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.how_often, android.R.layout.simple_spinner_item );
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item );

		/*
		// Makes the date part invisible and the description one as well only for the "equalizer"
		layout.findViewById(R.id.date_layout).setVisibility(View.GONE);
		if (v.getId()==R.id.equal) {
			layout.findViewById(R.id.description_layout).setVisibility(View.GONE);
			layout.findViewById(R.id.checkbox_layout).setVisibility(View.GONE);
		}

		final String account = accountTextField.getText().toString();
		money = (EditText) layout.findViewById(R.id.money_txt);
		description = (EditText) layout.findViewById(R.id.description_txt);
		description.setOnEditorActionListener(listener);

		builder.setView(layout).setTitle(R.string.dialog_title).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.cancel();
			}
		}).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {


			}
		}).show();
		 */
	}



	/** Listener to hide the keyboard when ENTER is pressed on entering the new account name */
	public OnEditorActionListener listener = new TextView.OnEditorActionListener() {
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (actionId == EditorInfo.IME_NULL || actionId == EditorInfo.IME_ACTION_DONE) {
				((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(description.getWindowToken(), 0);
			}
			return false;
		}
	};

	/** Closes the DB in case an operation didn't end properly */
	@Override
	public void onBackPressed() {
		setResult(Constants.RESULT_CODE_CHECKBOX);
		finish();
	};
}
