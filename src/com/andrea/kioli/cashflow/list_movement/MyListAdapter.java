/** Copyright 2012 by Lorenzo Marchiori
 * 
 * List adapter that takes care of properly filling the list in the home page for the account movements
 * 
 * @author kioli */

package com.andrea.kioli.cashflow.list_movement;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.andrea.kioli.cashflow.R;
import com.andrea.kioli.cashflow.utils.Challenge;

public class MyListAdapter extends ArrayAdapter<AccountObject> {

	private final ArrayList<AccountObject> items;
	private final Context context;
	private final Challenge challenge;

	public MyListAdapter(Challenge challenge, Context context, int textViewResourceId, ArrayList<AccountObject> items) {
		super(context, textViewResourceId, items);
		this.items = items;
		this.context = context;
		this.challenge = challenge;
	}

	@Override
	public View getView(int index, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.list_row_layout, null);
		}

		AccountObject data = items.get(index);

		if (data != null) {
			TextView list_amount = (TextView) v.findViewById(R.id.amount);
			TextView list_details = (TextView) v.findViewById(R.id.details);
			TextView list_date = (TextView) v.findViewById(R.id.date);
			LinearLayout list_category = (LinearLayout) v.findViewById(R.id.list_category);

			// set money
			if (list_amount != null) {
				list_amount.setText(Double.toString(data.expanse));
			}
			// set description
			if (list_details != null) {
				list_details.setText(data.description);
			}
			// set date
			if (list_date != null) {
				list_date.setText(data.date);
			}
			// set category
			if (list_category != null) {
				list_category.setBackgroundColor(challenge.allColors[data.category]);
			}

		}
		v.setId(data.ID);
		return v;
	}
}
