/** Copyright 2012 by Lorenzo Marchiori
 * 
 * Class that defines the object that populates the list
 * 
 * @author kioli */

package com.andrea.kioli.cashflow.list_movement;

public class AccountObject {

	public double expanse;
	public int ID, category;
	public String description, date;

	public AccountObject() {
		expanse = 0;
		description = "";
		date = "";
		category = 0;
		ID = 0;
	}

	/**
	 * @return the category
	 */
	public int getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(int category) {
		this.category = category;
	}

	/** @return the iD */
	public int getID() {
		return ID;
	}

	/** @param iD
	 *        the iD to set */
	public void setID(int iD) {
		ID = iD;
	}

	/** @return the expanse */
	public double getExpanse() {
		return expanse;
	}

	/** @param expanse
	 *        the expanse to set */
	public void setExpanse(double expanse) {
		this.expanse = expanse;
	}

	/** @return the description */
	public String getDescription() {
		return description;
	}

	/** @param description
	 *        the description to set */
	public void setDescription(String description) {
		this.description = description;
	}

	/** @return the date */
	public String getDate() {
		return date;
	}

	/** @param date
	 *        the date to set */
	public void setDate(String date) {
		this.date = date;
	}

}
