package com.andrea.kioli.cashflow.database.tables;

import android.net.Uri;
import android.provider.BaseColumns;
import com.andrea.kioli.cashflow.database.myContentProvider;

public class Movements implements BaseColumns {

	public Movements() {
	}

	public static final class MovementsAll implements BaseColumns {

		public static final String TABLE_NAME = "movements";
		public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.schipholsecurity.movements";
		public static final Uri TABLE_URI = Uri.parse("content://" + myContentProvider.AUTHORITY + "/" + TABLE_NAME);

		//public static final String ID = "AS _id";
		public static final String NAME = "name";
		public static final String PARENT_ACCOUNT_ID = "parentAccountID";
		public static final String VALUE = "value";
		public static final String MOVEMENT = "movement";
		public static final String DATE = "date";
		public static final String CATEGORY = "category";
	}
}