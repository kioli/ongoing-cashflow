package com.andrea.kioli.cashflow.database.tables;

import android.net.Uri;
import android.provider.BaseColumns;
import com.andrea.kioli.cashflow.database.myContentProvider;

public class Category implements BaseColumns {

	public Category() {
	}

	public static final class CategoryAll implements BaseColumns {

		public static final String TABLE_NAME = "category";
		public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.schipholsecurity.category";
		public static final Uri TABLE_URI = Uri.parse("content://" + myContentProvider.AUTHORITY + "/" + TABLE_NAME);

		// public static final String ID = "AS _id";
		public static final String NAME = "name";
		public static final String COLOR = "color";
	}
}