package com.andrea.kioli.cashflow.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import com.andrea.kioli.cashflow.R;
import com.andrea.kioli.cashflow.database.tables.Pigs.ListAccountsAll;
import com.andrea.kioli.cashflow.database.tables.Movements.MovementsAll;
import com.andrea.kioli.cashflow.utils.Constants;
import com.andrea.kioli.cashflow.utils.MyLog;

/** Class for managing the access to the DataBase as per writing, reading and deleting informations
 * @author kioli */
public class myContentProvider extends ContentProvider {

	public static String AUTHORITY;
	private static UriMatcher uriMatcher;
	private DBHelper dbHelper;				// Helper that creates the database starting from queries

	@Override
	public boolean onCreate() {

		AUTHORITY = getContext().getPackageName() + ".myContentProvider";
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(AUTHORITY, ListAccountsAll.TABLE_NAME, Constants.DB_TABLE_LIST_ACCOUNTS);
		uriMatcher.addURI(AUTHORITY, MovementsAll.TABLE_NAME, Constants.DB_TABLE_MOVEMENTS);

		dbHelper = DBHelper.getInstance(getContext());
		dbHelper.getWritableDatabase();

		return true;
	}

	/** Query data in db */
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		MyLog.i("DATABASE", "database queried");

		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		switch (uriMatcher.match(uri)) {
			case Constants.DB_TABLE_LIST_ACCOUNTS:
				qb.setTables(ListAccountsAll.TABLE_NAME);
				break;
			case Constants.DB_TABLE_MOVEMENTS:
				qb.setTables(MovementsAll.TABLE_NAME);
				break;
			default:
				throw new IllegalArgumentException(getContext().getResources().getString(R.string.ContentProviderUnknownURI) + uri);
		}

		SQLiteDatabase db = dbHelper.getWritableDatabase();
		Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);

		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}

	@Override
	public String getType(Uri uri) {
		switch (uriMatcher.match(uri)) {
			case Constants.DB_TABLE_LIST_ACCOUNTS:
				return ListAccountsAll.CONTENT_TYPE;
			case Constants.DB_TABLE_MOVEMENTS:
				return MovementsAll.CONTENT_TYPE;
			default:
				throw new IllegalArgumentException(getContext().getResources().getString(R.string.ContentProviderUnsupportedURI) + uri);
		}
	}

	/** Insert data to db */
	@Override
	public synchronized Uri insert(Uri uri, ContentValues values) throws SQLException {
		MyLog.i("DATABASE", "database insert");

		if (values != null) {
			SQLiteDatabase db = null;
			try {
				db = this.dbHelper.getWritableDatabase();
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (db != null) {
				db.beginTransaction();
				try {
					db.insert(uri.getLastPathSegment(), null, values);
					db.setTransactionSuccessful();
				} catch (Exception e) {
					e.printStackTrace();
				} catch (OutOfMemoryError e) {
					e.printStackTrace();
				} finally {
					db.endTransaction();
				}
			}
		}
		return uri;
	}

	@Override
	public int bulkInsert(Uri uri, ContentValues[] values) {
		MyLog.i("DATABASE", "database BULK insert");

		if (values != null) {
			SQLiteDatabase db = null;
			try {
				db = this.dbHelper.getWritableDatabase();
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (db != null) {
				db.beginTransaction();
				try {
					for (ContentValues cv : values) {
						db.replace(uri.getLastPathSegment(), null, cv);
					}

					db.setTransactionSuccessful();
				} catch (Exception e) {
					e.printStackTrace();
				} catch (OutOfMemoryError e) {
					e.printStackTrace();
				} finally {
					db.endTransaction();
				}
			}
		}
		return values.length;
	}

	/** Delete data in db */
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		MyLog.i("DATABASE", "database delete");

		SQLiteDatabase db = this.dbHelper.getWritableDatabase();
		int count;

		switch (uriMatcher.match(uri)) {
			case Constants.DB_TABLE_LIST_ACCOUNTS:
				count = db.delete(ListAccountsAll.TABLE_NAME, selection, selectionArgs);
				break;
			case Constants.DB_TABLE_MOVEMENTS:
				count = db.delete(MovementsAll.TABLE_NAME, selection, selectionArgs);
				break;
			default:
				throw new IllegalArgumentException(getContext().getResources().getString(R.string.ContentProviderUnknownURI) + uri);
		}

		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	/** Update data in db */
	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		SQLiteDatabase db = this.dbHelper.getWritableDatabase();
		int count;

		switch (uriMatcher.match(uri)) {
			case Constants.DB_TABLE_LIST_ACCOUNTS:
				count = db.update(ListAccountsAll.TABLE_NAME, values, selection, selectionArgs);
				break;
			case Constants.DB_TABLE_MOVEMENTS:
				count = db.update(MovementsAll.TABLE_NAME, values, selection, selectionArgs);
				break;
			default:
				throw new IllegalArgumentException(getContext().getResources().getString(R.string.ContentProviderUnknownURI) + uri);
		}

		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}
}
