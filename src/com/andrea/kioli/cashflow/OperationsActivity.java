/** Copyright 2012 by Lorenzo Marchiori
 * 
 * Class for defining new movements (minus, plus or equalizing)
 * 
 * @author kioli */

package com.andrea.kioli.cashflow;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.andrea.kioli.cashflow.database.tables.Movements.MovementsAll;
import com.andrea.kioli.cashflow.database.tables.Pigs.ListAccountsAll;
import com.andrea.kioli.cashflow.utils.BetterActivity;
import com.andrea.kioli.cashflow.utils.Challenge;
import com.andrea.kioli.cashflow.utils.Constants;
import com.andrea.kioli.cashflow.utils.DecimalWatcher;

public class OperationsActivity extends BetterActivity {

	Context context;
	Challenge challenge;
	Button button_ok, button_cancel;
	String accountName;
	int accountID;
	int typeOfButton;
	Cursor cursor;
	EditText money, description;
	CheckBox repeat_movement;
	final int MAX_NUMBER_MONEY = 6;
	final int MAX_DECIMAL_MONEY = 2;
	DecimalFormat twoDForm = new DecimalFormat("#.##");

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		challenge = (Challenge) getApplication();
		context = this;

		Intent intent = getIntent();
		Bundle b = intent.getExtras();
		accountName = b.getString(Constants.NAME_ACCOUNT);
		accountID = b.getInt(Constants.ID_ACCOUNT);
		typeOfButton = b.getInt(Constants.TYPE_OF_BUTTON);

		setContentView(R.layout.movement_dialog);
		init();
	}

	public void init() {

		findViewById(R.id.date_layout).setVisibility(View.GONE);
		if (typeOfButton == R.id.equal) {
			findViewById(R.id.description_layout).setVisibility(View.GONE);
			findViewById(R.id.checkbox_layout).setVisibility(View.GONE);
		}

		money = (EditText) findViewById(R.id.money_txt);
		description = (EditText) findViewById(R.id.description_txt);
		repeat_movement = (CheckBox) findViewById(R.id.fixed_movement);
		button_ok = (Button) findViewById(R.id.button_ok);
		button_cancel = (Button) findViewById(R.id.button_cancel);

		money.setOnEditorActionListener(listener);
		money.setFilters(new InputFilter[] {new DecimalWatcher(MAX_NUMBER_MONEY,MAX_DECIMAL_MONEY)});
		description.setOnEditorActionListener(listener);
		button_ok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (money.getText().toString().length() <= 0) {
					money.setText("0");
				}
				switch (typeOfButton) {
					case R.id.plus:
						addMoney(accountName, accountID, BigDecimal.valueOf(Double.parseDouble(money.getText().toString())), description.getText().toString());
						break;
					case R.id.minus:
						addMoney(accountName, accountID, BigDecimal.valueOf(-Double.parseDouble(money.getText().toString())), description.getText().toString());
						break;
					case R.id.equal:
						money.setOnEditorActionListener(listener);
						description.setText(R.string.equalized_txt);
						equalMoney(accountName, accountID, Double.parseDouble(money.getText().toString()), description.getText().toString());
						break;
					default:
						break;
				}
				setResult(Constants.RESULT_CODE_MAIN);
				finish();

			}
		});

		button_cancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				setResult(Constants.RESULT_CODE_MAIN);
				finish();
			}
		});

		repeat_movement.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				Intent i = new Intent(context, MultipleMovementActivity.class);
				startActivityForResult(i, Constants.REQUEST_CODE_CHECKBOX);
			}
		});
	}

	public void addMoney(String account, int accountID, BigDecimal cash, String description) {
		
		cash.setScale(2,BigDecimal.ROUND_CEILING);
		ContentValues trial = new ContentValues();
		trial.put(MovementsAll.NAME, account);
		trial.put(MovementsAll.PARENT_ACCOUNT_ID, accountID);
		trial.put(MovementsAll.VALUE, cash.doubleValue());
		trial.put(MovementsAll.MOVEMENT, description);
		trial.put(MovementsAll.DATE, challenge.df.format(challenge.c.getTime()));
		trial.put(MovementsAll.CATEGORY, 0);

		context.getContentResolver().insert(MovementsAll.TABLE_URI, trial);

		setNewTotal(account, cash.doubleValue());
	}

	public void equalMoney(String account, int accountID, double money, String description) {

		cursor = managedQuery(ListAccountsAll.TABLE_URI, new String[] { ListAccountsAll._ID, ListAccountsAll.TOTAL }, ListAccountsAll.NAME + "='"
				+ account + "'", null, ListAccountsAll._ID + " ASC");
		cursor.moveToNext();
		double difference_in_money = Double.valueOf(twoDForm.format(money - cursor.getDouble(cursor.getColumnIndex("total"))));

		ContentValues trial = new ContentValues();
		trial.put(MovementsAll.NAME, account);
		trial.put(MovementsAll.PARENT_ACCOUNT_ID, accountID);
		trial.put(MovementsAll.VALUE, difference_in_money);
		trial.put(MovementsAll.MOVEMENT, description.replace("'", "''"));
		trial.put(MovementsAll.DATE, challenge.df.format(challenge.c.getTime()));
		trial.put(MovementsAll.CATEGORY, 99);
		context.getContentResolver().insert(MovementsAll.TABLE_URI, trial);

		trial.clear();
		trial.put(ListAccountsAll.TOTAL, money);
		context.getContentResolver().update(ListAccountsAll.TABLE_URI, trial, "" + ListAccountsAll.NAME + "='" + account + "'", null);
	}

	public void setNewTotal(String account, double newMoney) {

		cursor = managedQuery(ListAccountsAll.TABLE_URI, new String[] { ListAccountsAll._ID, ListAccountsAll.TOTAL }, ListAccountsAll.NAME + "='"
				+ account + "'", null, ListAccountsAll._ID + " ASC");
		cursor.moveToNext();

		ContentValues trial = new ContentValues();
		trial.put(ListAccountsAll.TOTAL, cursor.getDouble(cursor.getColumnIndex("total")) + newMoney);
		context.getContentResolver().update(ListAccountsAll.TABLE_URI, trial, "" + ListAccountsAll.NAME + "='" + account + "'", null);

	}

	/** Listener to hide the keyboard when ENTER is pressed on entering the new account name */
	public OnEditorActionListener listener = new TextView.OnEditorActionListener() {
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if ((actionId == EditorInfo.IME_NULL || actionId == EditorInfo.IME_ACTION_DONE) && description.hasFocus()) {
				((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE))
						.hideSoftInputFromWindow(description.getWindowToken(), 0);
			} else if ((actionId == EditorInfo.IME_NULL || actionId == EditorInfo.IME_ACTION_DONE) && money.hasFocus() && typeOfButton == R.id.equal) {
				((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(money.getWindowToken(), 0);
			}
			return false;
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.RESULT_CODE_CHECKBOX) {
			// refreshViewWithAccountData(name_account.getText().toString());
		}
	};

	/** Closes the DB in case an operation didn't end properly */
	@Override
	public void onBackPressed() {
		setResult(Constants.RESULT_CODE_MAIN);
		finish();
	};
}