/** Copyright 2012 by Lorenzo Marchiori
 * 
 * Main activity that shows the home screen
 * 
 * @author kioli */

package com.andrea.kioli.cashflow;

import java.util.ArrayList;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.andrea.kioli.cashflow.database.tables.Movements.MovementsAll;
import com.andrea.kioli.cashflow.database.tables.Pigs.ListAccountsAll;
import com.andrea.kioli.cashflow.graph.MyGraph;
import com.andrea.kioli.cashflow.list_movement.AccountObject;
import com.andrea.kioli.cashflow.list_movement.MyListAdapter;
import com.andrea.kioli.cashflow.utils.BetterActivity;
import com.andrea.kioli.cashflow.utils.Challenge;
import com.andrea.kioli.cashflow.utils.Constants;

public class MainActivity extends BetterActivity {

	private TextView name_account, money_account;
	private ImageButton add_account;
	private ImageButton delete_account;
	private ImageButton plus;
	private ImageButton minus;
	private ImageButton equal;
	private ImageButton graph;
	private Context context;
	private Challenge challenge;
	private String[] accountItems;
	private int chosen_account = 1;
	private int listPointer = 0;
	private ListView list;
	private EditText add_dialog_input;
	private AlertDialog.Builder builder;
	private Cursor cursor;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		challenge = (Challenge) getApplication();

		setContentView(R.layout.main);
		name_account = (TextView) findViewById(R.id.account_name);
		money_account = (TextView) findViewById(R.id.money_amount);
		add_account = (ImageButton) findViewById(R.id.btn_add_account);
		delete_account = (ImageButton) findViewById(R.id.btn_remove_account);
		plus = (ImageButton) findViewById(R.id.plus);
		minus = (ImageButton) findViewById(R.id.minus);
		equal = (ImageButton) findViewById(R.id.equal);
		graph = (ImageButton) findViewById(R.id.graph);
		list = (ListView) findViewById(R.id.list_of_movements);

		init();
	}

	public void init() {

		// Checks for accounts existence and in case not it makes the user create one
		cursor = context.getContentResolver().query(ListAccountsAll.TABLE_URI, new String[] { ListAccountsAll._ID }, null, null,
				ListAccountsAll._ID + " ASC");
		if (cursor.moveToNext()) {
			chosen_account = cursor.getInt(cursor.getColumnIndex(ListAccountsAll._ID));
			refreshViewWithAccountData(chosen_account);
		} else {
			createAccount();
		}

		delete_account.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				deleteAccount();
			}
		});

		name_account.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				getAccountsList();
			}
		});

		add_account.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				createAccount();
			}
		});

		plus.setOnClickListener(OperationListener);
		minus.setOnClickListener(OperationListener);
		equal.setOnClickListener(OperationListener);
		graph.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

//				cursor = context.getContentResolver().query(MovementsAll.TABLE_URI,
//						new String[] { MovementsAll._ID, MovementsAll.NAME, MovementsAll.VALUE, MovementsAll.MOVEMENT, MovementsAll.DATE },
//						MovementsAll.PARENT_ACCOUNT_ID + "=" + chosen_account + "", null, MovementsAll._ID + " DESC");
//
//				ArrayList<String> descript = new ArrayList<String>();
//				ArrayList<String> expans = new ArrayList<String>();
//
//				while (cursor.moveToNext()) {
//					descript.add(cursor.getString(cursor.getColumnIndex(MovementsAll.MOVEMENT)));
//					expans.add(String.valueOf(cursor.getDouble(cursor.getColumnIndex(MovementsAll.VALUE))));
//				}

				//GraphMaker graph = new GraphMaker(challenge, context, descript, expans);
				//startActivity(graph.getIntent());
				
				Intent i=new Intent(context,MyGraph.class);
				i.putExtra("account", name_account.getText());
				i.putExtra("accountID", chosen_account);
				context.startActivity(i);
			}
		});
	}

	/** Shows a dialog with all the current accounts saved in the DB */
	public void getAccountsList() {

		cursor = context.getContentResolver().query(ListAccountsAll.TABLE_URI,
				new String[] { ListAccountsAll._ID, ListAccountsAll.NAME, ListAccountsAll.TOTAL }, null, null, ListAccountsAll._ID + " ASC");

		// Creates an array with the accounts
		accountItems = new String[cursor.getCount()];
		for (int i = 0; i < cursor.getCount(); i++) {
			cursor.moveToNext();
			accountItems[i] = cursor.getString(cursor.getColumnIndex(ListAccountsAll.NAME));
		}

		// Creates the Dialog with the options
		builder = new AlertDialog.Builder(context);
		builder.setSingleChoiceItems(accountItems, listPointer, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				listPointer = which;
				cursor.moveToPosition(which);
				chosen_account = cursor.getInt(cursor.getColumnIndex(ListAccountsAll._ID));
				refreshViewWithAccountData(chosen_account);
				dialog.cancel();
			}
		});

		builder.show();
	}

	/** Method that creates a new account and saves it in the DB */
	public void createAccount() {

		View layout = LayoutInflater.from(context).inflate(R.layout.add_account_dialog, null);

		add_dialog_input = (EditText) layout.findViewById(R.id.editText);
		add_dialog_input.setOnEditorActionListener(listener);

		builder = new AlertDialog.Builder(this);
		final AlertDialog d = builder.setTitle(R.string.dialog_insert_account_title).setView(layout).setPositiveButton(android.R.string.ok, null)
				.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.cancel();
					}
				}).show();

		// Listener on the positive button that shows a warning in case no name for the account is typed
		d.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (add_dialog_input.getText().length() <= 0) {
					Toast.makeText(context, R.string.empty_name, Toast.LENGTH_SHORT).show();
				} else {
					cursor = context.getContentResolver().query(ListAccountsAll.TABLE_URI,
							new String[] { ListAccountsAll._ID, ListAccountsAll.NAME }, null, null, ListAccountsAll._ID + " ASC");

					// Check if the typed name is already present
					boolean account_found = false;
					while (cursor.moveToNext()) {
						if (cursor.getString(cursor.getColumnIndex(ListAccountsAll.NAME)).equalsIgnoreCase(add_dialog_input.getText().toString())) {
							account_found = true;
						}
					}

					// Insert the name in the DB or says it is present already
					if (account_found) {
						Toast.makeText(context, R.string.double_account, Toast.LENGTH_SHORT).show();
					} else {
						insertAccount();
					}
					d.dismiss();
				}
			}
		});
		d.setOnCancelListener(new OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				cursor = context.getContentResolver().query(ListAccountsAll.TABLE_URI, new String[] { ListAccountsAll._ID }, null, null,
						ListAccountsAll._ID + " ASC");
				if (cursor.moveToNext()) {
					cursor.moveToPosition(listPointer);
					chosen_account = cursor.getInt(cursor.getColumnIndex(ListAccountsAll._ID));
					refreshViewWithAccountData(chosen_account);
				} else {
					name_account.setText("");
					money_account.setText("");
					View empty = LayoutInflater.from(context).inflate(R.layout.list_row_layout, null);
					list.setEmptyView(empty);
				}

			}
		});
	}

	/** Deletes the current account from the DB */
	public void deleteAccount() {

		cursor = context.getContentResolver().query(ListAccountsAll.TABLE_URI, new String[] { ListAccountsAll._ID }, null, null,
				ListAccountsAll._ID + " ASC");
		if (cursor.moveToNext()) {

			builder = new AlertDialog.Builder(this);

			String text;
			if (!name_account.getText().equals(Constants.AccWall) && !name_account.getText().equals(Constants.AccBank)) {
				text = getResources().getString(R.string.dialog_delete_account_txt, name_account.getText());
			} else {
				text = getResources().getString(R.string.dialog_delete_account_warning_txt, name_account.getText());
			}

			builder.setTitle(R.string.dialog_delete_account_title).setMessage(text)
			.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					if (!name_account.getText().equals(Constants.AccWall) && !name_account.getText().equals(Constants.AccBank)) {
						removeAccount(chosen_account);
					} else {
						resetBasicAccount(chosen_account);
					}
				}
			}).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.cancel();
				}
			}).show();
		} else {
		}
	}

	/** Inserts the new account in the AccountList table
	 * @param cursor */
	public void insertAccount() {
		ContentValues trial = new ContentValues();
		trial.put(ListAccountsAll.NAME, add_dialog_input.getText().toString());
		trial.put(ListAccountsAll.TOTAL, "0");

		context.getContentResolver().insert(ListAccountsAll.TABLE_URI, trial);
		cursor = context.getContentResolver().query(ListAccountsAll.TABLE_URI, new String[] { ListAccountsAll._ID }, null, null,
				ListAccountsAll._ID + " ASC");

		cursor.moveToLast();
		chosen_account = cursor.getInt(cursor.getColumnIndex(ListAccountsAll._ID));
		listPointer = cursor.getCount() - 1;
		refreshViewWithAccountData(chosen_account);
	}

	/** Properly remove a table from the DB removing the related table and the entry from the AccountList table
	 * @param cursor */
	public void removeAccount(int account) {

		cursor = context.getContentResolver().query(ListAccountsAll.TABLE_URI, new String[] { ListAccountsAll._ID }, null, null, null);

		int position = 0;
		while (cursor.moveToNext()) {
			if (cursor.getInt(cursor.getColumnIndex(ListAccountsAll._ID)) == account) {
				position = cursor.getPosition();
			}
		}

		context.getContentResolver().delete(ListAccountsAll.TABLE_URI, "" + ListAccountsAll._ID + "=" + account + "", null);
		context.getContentResolver().delete(MovementsAll.TABLE_URI, "" + MovementsAll.PARENT_ACCOUNT_ID + "=" + account + "", null);

		if (position == 0) {
			listPointer = position;
		} else {
			listPointer = position - 1;
		}

		// Checks for accounts existence and in case not it makes the user create one
		cursor = context.getContentResolver().query(ListAccountsAll.TABLE_URI, new String[] { ListAccountsAll._ID }, null, null,
				ListAccountsAll._ID + " ASC");
		if (cursor.moveToNext()) {
			cursor.moveToPosition(listPointer);
			chosen_account = cursor.getInt(cursor.getColumnIndex(ListAccountsAll._ID));
			refreshViewWithAccountData(chosen_account);
		} else {
			createAccount();
		}
	}

	/** Extracts from the DB the data of the current account and shows them in the home screen
	 * @param chosenAccount */
	public void refreshViewWithAccountData(int chosenAccount) {

		// Extracts the name of an account with a certain ID
		cursor = context.getContentResolver().query(ListAccountsAll.TABLE_URI, new String[] { ListAccountsAll._ID, ListAccountsAll.NAME },
				"" + ListAccountsAll._ID + "=" + chosenAccount + "", null, null);
		cursor.moveToNext();
		String selectedAccount = cursor.getString(cursor.getColumnIndex(ListAccountsAll.NAME));

		// Extracts the movements related to that account
		cursor = context.getContentResolver().query(MovementsAll.TABLE_URI,
				new String[] { MovementsAll._ID, MovementsAll.NAME, MovementsAll.VALUE, MovementsAll.MOVEMENT, MovementsAll.DATE },
				MovementsAll.PARENT_ACCOUNT_ID + "=" + chosenAccount + "", null, MovementsAll._ID + " DESC");

		ArrayList<AccountObject> description_items = new ArrayList<AccountObject>();

		while (cursor.moveToNext()) {
			AccountObject temp = new AccountObject();
			temp.expanse = cursor.getDouble(cursor.getColumnIndex(MovementsAll.VALUE));
			temp.description = cursor.getString(cursor.getColumnIndex(MovementsAll.MOVEMENT));
			temp.date = cursor.getString(cursor.getColumnIndex(MovementsAll.DATE));
			temp.ID = cursor.getInt(cursor.getColumnIndex(MovementsAll._ID));
			description_items.add(temp);
		}

		double total = getTotal(chosenAccount);

		// Gives the proper color to the total amount of money
		if (total < 0) {
			money_account.setTextColor(getResources().getColor(R.color.red));
		} else if (total > 0) {
			money_account.setTextColor(getResources().getColor(R.color.green));
		} else {
			money_account.setTextColor(getResources().getColor(R.color.gray));
		}

		// Updates the entries in the list with DB data
		MyListAdapter list_adapter = new MyListAdapter(challenge, context, R.layout.list_row_layout, description_items);
		list.setAdapter(list_adapter);
		list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
				Intent i = new Intent(context, EditMovementActivity.class);
				i.putExtra(Constants.NAME_ACCOUNT, name_account.getText().toString());
				i.putExtra(Constants.LINE_ID, view.getId());
				startActivityForResult(i, Constants.REQUEST_CODE_MAIN);
			}
		});

		money_account.setText(String.valueOf(total));
		name_account.setText(selectedAccount);
	}

	public double getTotal(int account) {

		cursor = context.getContentResolver().query(ListAccountsAll.TABLE_URI, new String[] { ListAccountsAll.TOTAL },
				"" + ListAccountsAll._ID + "=" + account + "", null, null);
		cursor.moveToNext();
		return cursor.getDouble(cursor.getColumnIndex(ListAccountsAll.TOTAL));
	}

	/** Listener to hide the keyboard when ENTER is pressed on entering the new account name */
	public OnEditorActionListener listener = new TextView.OnEditorActionListener() {
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if ((actionId == EditorInfo.IME_NULL || actionId == EditorInfo.IME_ACTION_DONE) && add_dialog_input.hasFocus()) {
				((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(add_dialog_input.getWindowToken(), 0);
			}
			return false;
		}
	};

	public OnClickListener OperationListener = new OnClickListener() {
		public void onClick(View v) {
			Intent i = new Intent(context, OperationsActivity.class);
			i.putExtra(Constants.NAME_ACCOUNT, name_account.getText().toString());
			i.putExtra(Constants.ID_ACCOUNT, chosen_account);
			i.putExtra(Constants.TYPE_OF_BUTTON, v.getId());
			startActivityForResult(i, Constants.REQUEST_CODE_MAIN);
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.RESULT_CODE_MAIN) {
			refreshViewWithAccountData(chosen_account);
		}
	}

	public void resetBasicAccount(int account) {
		ContentValues trial = new ContentValues();
		trial.put(ListAccountsAll.TOTAL, "0");

		context.getContentResolver().delete(MovementsAll.TABLE_URI, "" + MovementsAll.PARENT_ACCOUNT_ID + "=" + account + "", null);
		context.getContentResolver().update(ListAccountsAll.TABLE_URI, trial, "" + ListAccountsAll._ID + "=" + account + "", null);

		refreshViewWithAccountData(account);
	}
}