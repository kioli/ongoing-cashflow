/* CashFlow Project
 * 
 * Class Challenge (extends Application)
 * 
 * Use:
 * This class extends Application, it means that its variables will be shared throughout the application
 * it's used for this purpose to keep track of global changes
 *
 */

package com.andrea.kioli.cashflow.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import com.andrea.kioli.cashflow.R;

public class Challenge extends Application {

	public SQLiteDatabase myDB;
	public Calendar c;
	public SimpleDateFormat df;
	public String today;

	// Standard tables always created as default
	public String TableListAccounts = "Accounts";
	public String TableMovements = "Movements";
	public String AccWall = "Wallet";
	public String AccBank = "Bank";

	public boolean firstTime = true;	// checks whether is the first time for the app to be launched
	public SharedPreferences prefs;		// preferences shared within all the application

	public ArrayList<String> calendarDays;
	public String [] calendarMonths;
	public int[] allColors;

	// Once the Application is launched it sets the starting date of the fair to the 9th of January 2012 at 11.00
	@Override
	public void onCreate() {

		prefs = getSharedPreferences(Constants.PREFERENCES_NAME, Context.MODE_PRIVATE);	// gets the shared preferences

		c = Calendar.getInstance();
		df = new SimpleDateFormat("dd-MM-yy HH:MM");
		today = df.format(c.getTime());

		// Creation of the arrays for repeating events
		calendarDays = new ArrayList<String>();
		for (int i=0; i<31; i++) {
			calendarDays.add(Integer.toString(i+1));
		}
		//calendarMonths = getResources().getStringArray(R.string.months);

		allColors = getApplicationContext().getResources().getIntArray(R.array.graph_colors);

		if (isFirstTime()) {
			/*
			// Initialize the database
			myDB = this.openOrCreateDatabase(Constants.DATABASE_NAME, MODE_PRIVATE, null);

			// Create the three basic tables in the database
			myDB.execSQL("CREATE TABLE IF NOT EXISTS " + TableListAccounts + Constants.TableListAccounts);
			myDB.execSQL("CREATE TABLE IF NOT EXISTS " + TableMovements + Constants.TableMovements);

			// Insert data into the AccountList Table
			myDB.execSQL("INSERT INTO " + TableListAccounts + " (name, total)" + " VALUES ('" + AccWall + "', 0);");
			myDB.execSQL("INSERT INTO " + TableListAccounts + " (name, total)" + " VALUES ('" + AccBank + "', 0);");

			// Insert first entry in the Movements table
			myDB.execSQL("INSERT INTO " + TableMovements + " (name, value, movement, date)" + " VALUES ('" + AccWall + "', 0, '', '" + today + "');");
			myDB.execSQL("INSERT INTO " + TableMovements + " (name, value, movement, date)" + " VALUES ('" + AccBank + "', 0, '', '" + today + "');");

			myDB.close();
			setFirstTime(false);*/
		}
	}

	public void setFirstTime(boolean status) {
		if (prefs != null) {
			Editor edit = prefs.edit();
			edit.putBoolean(Constants.SP_FIRST_TIME_LABEL, status);
			edit.commit();
		}
	}

	public boolean isFirstTime() {
		if (prefs != null && prefs.getBoolean(Constants.SP_FIRST_TIME_LABEL, true)) {
			return prefs.getBoolean(Constants.SP_FIRST_TIME_LABEL, true);
		} else {
			return false;
		}
	}

	/*public void createDataBase() {
		// Initialize the database
		myDB = this.openOrCreateDatabase(Constants.DATABASE_NAME, MODE_PRIVATE, null);

		// Create the three basic tables in the database
		myDB.execSQL("CREATE TABLE IF NOT EXISTS " + TableListAccounts + Constants.TableListAccounts);
		myDB.execSQL("CREATE TABLE IF NOT EXISTS " + TableMovements + Constants.TableMovements);

		// Insert data into the AccountList Table
		myDB.execSQL("INSERT INTO " + TableListAccounts + " (name, total)" + " VALUES ('" + AccWall + "', 0);");
		myDB.execSQL("INSERT INTO " + TableListAccounts + " (name, total)" + " VALUES ('" + AccBank + "', 0);");

		// Insert first entry in the Movements table
		myDB.execSQL("INSERT INTO " + TableMovements + " (name, value, movement, date)" + " VALUES ('" + AccWall + "', 0, '', '" + today + "');");
		myDB.execSQL("INSERT INTO " + TableMovements + " (name, value, movement, date)" + " VALUES ('" + AccBank + "', 0, '', '" + today + "');");

		myDB.close();
	}*/
}