package com.andrea.kioli.cashflow.utils;

import java.util.ArrayList;
import org.achartengine.ChartFactory;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import android.content.Context;
import android.content.Intent;

public class GraphMaker {

	private final ArrayList<Integer> colors = new ArrayList<Integer>();
	private Intent intent = null;

	public GraphMaker(Challenge challenge, Context context, ArrayList<String> types, ArrayList<String> values) {

		if (types.size() == values.size()) {

			CategorySeries categorySeries = new CategorySeries("Title");

			for (int i = 0; i < types.size(); i++) {
				colors.add(challenge.allColors[i]);
				categorySeries.add(types.get(i), Double.parseDouble(values.get(i)));
			}
			DefaultRenderer renderer = buildCategoryRenderer(colors);
			intent = ChartFactory.getPieChartIntent(context, categorySeries, renderer, "");
		}
	}

	protected DefaultRenderer buildCategoryRenderer(ArrayList<Integer> list_colors) {
		DefaultRenderer renderer = new DefaultRenderer();
		for (int color : list_colors) {
			SimpleSeriesRenderer r = new SimpleSeriesRenderer();
			r.setColor(color);
			renderer.addSeriesRenderer(r);
		}
		return renderer;
	}

	public Intent getIntent() {
		return intent;
	}
}