package com.andrea.kioli.cashflow.utils;

public class Constants {

	// Splash screen durations
	public static final int SPLASH_STEP = 100;
	public static final int SPLASH_TIME = 2000;

	public static final int DB_TABLE_LIST_ACCOUNTS = 0;
	public static final int DB_TABLE_MOVEMENTS = 1;

	public static final int REQUEST_CODE_MAIN = 0;
	public static final int REQUEST_CODE_CHECKBOX = 1;
	public static final int RESULT_CODE_MAIN = 2;
	public static final int RESULT_CODE_CHECKBOX = 3;

	// Shared Preference labels
	public final static String PREFERENCES_NAME = "CashFlowPref";
	public final static String SP_FIRST_TIME_LABEL = "first_time";
	public final static String SP_DATABASE_LABEL = "name of the database";

	// Database
	public final static String AccWall = "Wallet";
	public final static String AccBank = "Bank";
	public final static String DATABASE_NAME = "CashFlow Database";

	public final static String TableListAccounts = " (name TEXT NOT NULL, total INTEGER);";
	public final static String TableMovements = " (name TEXT NOT NULL, value double, movement TEXT, date TEXT, FOREIGN KEY(name) REFERENCES Accounts(name));";

	// Labels for intents
	public final static String NAME_ACCOUNT = "name account";
	public final static String ID_ACCOUNT = "ID account";
	public final static String TYPE_OF_BUTTON = "type of button";
	public final static String LINE_VALUE = "line's value";
	public final static String LINE_DESCRIPTION = "line's description";
	public final static String LINE_DATE = "line's date";
	public final static String LINE_ID = "line's ID";
}