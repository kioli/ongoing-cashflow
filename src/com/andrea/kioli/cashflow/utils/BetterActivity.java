/* SchipolSecurity Project
 * Class BetterActivity (extends Application)
 * Use:
 * This class extends Activity and it's used for shaping some of the pre-existent
 * methods of class Activity basing the new ones on to our needs
 *
 */
package com.andrea.kioli.cashflow.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;
import com.andrea.kioli.cashflow.R;

public abstract class BetterActivity extends Activity {

	// The top level content view.
	private ViewGroup m_contentView = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		System.gc();
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		System.gc();
	}

	@Override
	public void setContentView(int layoutResID) {
		setContentView(LayoutInflater.from(this).inflate(layoutResID, null));
	}

	@Override
	public void setContentView(View view) {
		super.setContentView(view);
		m_contentView = (ViewGroup) view;
	}

	@Override
	public void setContentView(View view, LayoutParams params) {
		super.setContentView(view, params);
		m_contentView = (ViewGroup) view;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// Fixes android memory issue 8488:
		// http://code.google.com/p/android/issues/detail?id=8488
		nullViewDrawablesRecursive(m_contentView);
		m_contentView = null;
		System.gc();
	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		Window window = getWindow();
		window.setFormat(PixelFormat.RGBA_8888);
	}

	private void nullViewDrawablesRecursive(View view) {
		if (view != null) {
			try {
				ViewGroup viewGroup = (ViewGroup) view;
				int childCount = viewGroup.getChildCount();
				for (int index = 0; index < childCount; index++) {
					View child = viewGroup.getChildAt(index);
					nullViewDrawablesRecursive(child);
				}
			} catch (Exception e) {
			}
			nullViewDrawable(view);
		}
	}

	private void nullViewDrawable(View view) {
		try {
			view.setBackgroundDrawable(null);
		} catch (Exception e) {
		}
		try {
			ImageView imageView = (ImageView) view;
			imageView.setImageDrawable(null);
			imageView.setBackgroundDrawable(null);
		} catch (Exception e) {
		}
	}

	@Override
	public void onBackPressed() {
		//super.onBackPressed();
		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setTitle(getResources().getText(R.string.terminate_dialog_title))
		.setMessage(getResources().getText(R.string.terminate_dialog_text))
		.setPositiveButton(getResources().getText(android.R.string.yes), new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				BetterActivity.this.finish();
				System.runFinalizersOnExit(true);
				System.exit(0);
			}
		})
		.setNegativeButton(getResources().getText(android.R.string.no), null)
		.show();
	}
}
