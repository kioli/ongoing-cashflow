package com.andrea.kioli.cashflow;

import java.text.DecimalFormat;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.andrea.kioli.cashflow.database.tables.Pigs.ListAccountsAll;
import com.andrea.kioli.cashflow.database.tables.Movements.MovementsAll;
import com.andrea.kioli.cashflow.utils.BetterActivity;
import com.andrea.kioli.cashflow.utils.Challenge;
import com.andrea.kioli.cashflow.utils.Constants;

public class EditMovementActivity extends BetterActivity {

	Context context;
	Challenge challenge;
	TextView accountTextField;
	Cursor cursor;
	Button button_ok, button_cancel;
	DecimalFormat twoDForm = new DecimalFormat("#.##");

	int rowID;
	EditText money, description, date;
	CheckBox repeat_movement;
	String account;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		challenge = (Challenge) getApplication();
		context = this;

		Intent intent = getIntent();
		Bundle b = intent.getExtras();
		account = b.getString(Constants.NAME_ACCOUNT);
		rowID = b.getInt(Constants.LINE_ID);

		setContentView(R.layout.movement_dialog);
		init();
	}

	public void init() {
		money = (EditText) findViewById(R.id.money_txt);
		description = (EditText) findViewById(R.id.description_txt);
		date = (EditText) findViewById(R.id.date_txt);
		repeat_movement = (CheckBox) findViewById(R.id.fixed_movement);
		button_ok = (Button) findViewById(R.id.button_ok);
		button_cancel = (Button) findViewById(R.id.button_cancel);

		cursor = managedQuery(MovementsAll.TABLE_URI, new String[] { MovementsAll.VALUE, MovementsAll.MOVEMENT, MovementsAll.DATE },
				MovementsAll._ID + "=" + rowID + "", null, null);
		cursor.moveToNext();

		money.setText(Double.toString(cursor.getDouble(cursor.getColumnIndex(MovementsAll.VALUE))));
		description.setText(cursor.getString(cursor.getColumnIndex(MovementsAll.MOVEMENT)));
		date.setText(cursor.getString(cursor.getColumnIndex(MovementsAll.DATE)));

		date.setOnEditorActionListener(listener);

		button_ok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				modifyEntry(account, Double.parseDouble(money.getText().toString()), description.getText().toString(), date.getText().toString(),
						rowID);
				setResult(Constants.RESULT_CODE_MAIN);
				finish();
			}
		});

		button_cancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				setResult(Constants.RESULT_CODE_MAIN);
				finish();
			}
		});
	}

	public void modifyEntry(String account, double money, String description, String date, int rowID) {

		ContentValues trial = new ContentValues();
		trial.put(MovementsAll.VALUE, twoDForm.format(money));
		trial.put(MovementsAll.MOVEMENT, description);
		trial.put(MovementsAll.DATE, date);

		context.getContentResolver().update(MovementsAll.TABLE_URI, trial, "" + MovementsAll._ID + "=" + rowID + "", null);

		setNewTotal(account, money);
	}

	public void setNewTotal(String account, double newMoney) {

		cursor = managedQuery(MovementsAll.TABLE_URI, new String[] { MovementsAll._ID, MovementsAll.NAME, MovementsAll.VALUE, MovementsAll.MOVEMENT,
				MovementsAll.DATE }, MovementsAll.NAME + "='" + account + "'", null, MovementsAll._ID + " ASC");

		double newTotal = 0;
		while (cursor.moveToNext()) {
			newTotal += cursor.getDouble(cursor.getColumnIndex("value"));
		}

		ContentValues trial = new ContentValues();
		trial.put(ListAccountsAll.TOTAL, newTotal);
		context.getContentResolver().update(ListAccountsAll.TABLE_URI, trial, "" + MovementsAll.NAME + "='" + account + "'", null);
	}

	/** Listener to hide the keyboard when ENTER is pressed on entering the new account name */
	public OnEditorActionListener listener = new TextView.OnEditorActionListener() {
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (actionId == EditorInfo.IME_NULL || actionId == EditorInfo.IME_ACTION_DONE) {
				((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(date.getWindowToken(), 0);
			}
			return false;
		}
	};

	/** Closes the DB in case an operation didn't end properly */
	@Override
	public void onBackPressed() {
		setResult(Constants.RESULT_CODE_MAIN);
		finish();
	};
}
